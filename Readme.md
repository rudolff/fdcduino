# Firmware for an emulator of WD1793 (russian КР1818ВГ93) for zx-spectrum

This is a hardware emulator of Floppy Disk Controller based on an arduino board. Uses SD card to store floppy disk images.

- Schematics: https://easyeda.com/vitalian1980/fdcduino 
- Discussion (russian): https://zx-pk.ru/threads/30269-emulyator-kontrollera-diskovoda-beta-disk-na-avr.html
