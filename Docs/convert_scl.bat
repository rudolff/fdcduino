@echo off 
setlocal
echo Searching for new .SCL files...

for %%F in (*.SCL) do (
    call :subroutine %%F %%~nF
)
GOTO :eof

:subroutine
 set outfile=%2.TRD

 if not exist %outfile% (
   echo %outfile%
   scl2trd %1 %outfile%
   mv %1 OTHERS/%1
 )
 GOTO :eof
